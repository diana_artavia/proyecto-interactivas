<?php
include_once("includes/bd.php");
include_once("includes/mcript.php");
include_once("session.php");


if(isset($_POST["actualizar"])){

    $file = $_FILES["imagen"]["name"];
    if($file != null){
        $url_temp = $_FILES["imagen"]["tmp_name"];
        $url_insert = dirname(__FILE__) . "/imgRecetas";
        $url_target = str_replace('\\', '/', $url_insert). '/' . $file;

        if(!file_exists($url_insert)){
            mkdir($url_insert, 0777, true);
        };

        if(move_uploaded_file($url_temp, $url_target)){
            "El archivo " . htmlspecialchars(basename($file)) . " ha sido cargado con éxito.";
        } else {
            echo "Ha habido un error al cargar tu archivo.";
        }
    }else{
        $file = "";
    }
    

    $nombre = $_POST['nombre'];
    $usuario = $_POST['usuario'];
    $correo = $_POST['correo'];
    $perfil = $_POST['perfil'];
    $imagen = $file;
    $passw = $encriptar($_POST['passw']);
    $recetas_g = $_POST['recetas_g'];
    $recetas_v = $_POST['recetas_v'];
    
    $result = $database -> insert("usuario_tb", [
        "nombre_usuario" => $usuario,
        "correo" => $correo,
        "passw" => $passw,
        "perfil" => $perfil,
        "nombre" => $nombre,
        "img_perfil" => $imagen,
        "recetas_g" => $recetas_g,
        "session" => '0',
        "recetas_v" => $recetas_v
        ]);

        if(!$result){
            die("Query failed");
        }

        $_SESSION['mensaje'] = "Usuario guardado correctamente!";
        $_SESSION['mensaje_tipo'] = 'success';
        header("location: usuarios.php");

    }

?>