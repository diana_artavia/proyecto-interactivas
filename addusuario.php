<title>Agregar receta | Veggie Taste</title>
<?php
include_once 'includes/navbar.php';
include("session.php");

?>


<main>
    <div class="row">
        <div class="col-info mt-5">
            <h1 class="text-green mt-5 ml-5 mb-5">Nuevo usuario</h1>
        </div>
    </div>
    <form class="margin-form" action="saveusuario.php" method="post" enctype="multipart/form-data">
    <div class="row mb-4">
            <div class="col-4">
                <label for="nombre">Nombre:</label>
                <input id="nombre" class="form-item" type="text" name="nombre" placeholder="Nombre" required>
            </div>
            <div class="col-4">
                <Label for="img" >Imagen: </Label>
                <input id="img" class="form-item" accept=".png, .jpg" type="file" name="imagen">
            </div>
            <div class="col-4">
                <Label for="img" >Contraseña: </Label>
                <input id="passw" class="form-item" type="password" name="passw" placeholder="Contraseña" required>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-4">
                <label for="nombre">Correo:</label>
                <input id="correo" class="form-item" type="email" name="correo" placeholder="Correo" required>
            </div>
            <div class="col-4">
                <label for="nombre">Perfil:</label>
                <select class="form-item selectores" name="perfil" id="perfil" require>
                        <option value="Consulta">Consulta</option>
                        <option value="Administrador">Administrador</option>
                </select>
            </div>
            <div class="col-4">
                <label for="usuario">Usuario:</label>
                <input id="usuario" class="form-item" type="text" name="usuario" placeholder="Usuario" required>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-6">
                <div class="col-1"><label>Recetas guardadas:</label></div>
                <textarea id="receta_g" class="text-area" name="recetas_g" placeholder="Recetas guardadas: ejemplo 1,2,3"></textarea>
            </div>
            <div class="col-6">
                <div class="col-1"><label>Recetas votadas:</label></div>
                <textarea id="receta_v" class="text-area" name="recetas_v" placeholder="Recetas votadas: ejemplo 1,2,3"></textarea>
            </div>
        </div>
        <div class="row text-center mb-5 pb-5">
            <div class="col-6">
                <input class="btn-submit" type="button" onclick="history.back()" name="Atras" value="Atrás">
            </div>
            <div class="col-6">
                <input class="btn-submit" type="submit" name="actualizar" value="Actualizar">
            </div>
        </div>
    </form>
</main>