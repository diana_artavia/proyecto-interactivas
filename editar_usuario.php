<?php
include_once('includes/bd.php');
include_once('includes/navbar.php');


?>
<link rel="stylesheet" href="css/administrador.css">

<?php
if (isset($_GET['uid'])) {
    $id = $_GET['uid'];

    $result = $database->select("usuario_tb", "*", ['uid' => $id]);

    if (count($result) == 1) {
        $usuario = $result[0]['nombre_usuario'];
        $correo = $result[0]['correo'];
        $perfil = $result[0]['perfil'];
        $nombre = $result[0]['nombre'];
        $imagen = $result[0]['img_perfil'];
        $recetas_g = $result[0]['recetas_g'];
        $recetas_v = $result[0]['recetas_v'];
    }
}

if (isset($_POST['actualizar'])) {
    $id = $_GET['uid'];
    $usuario = $_POST['usuario'];
    $correo = $_POST['correo'];
    $perfil = $_POST['perfil'];
    $nombre = $_POST['nombre'];
    $imagen = $_POST['imagen'];
    $recetas_g = $_POST['recetas_g'];
    $recetas_v = $_POST['recetas_v'];

    $update = $database->update("usuario_tb", [
        "nombre_usuario" => $usuario,
        "correo" => $correo,
        "perfil" => $perfil,
        "nombre" => $nombre,
        "img_perfil" => $imagen,
    ], ["uid" => $id]);

    header("Location: usuarios.php");
}

?>

<main>
    <div class="row">
        <div class="col-info mt-5">
            <h1 class="text-green mt-5 ml-5 mb-5">Editar usuario</h1>
        </div>
    </div>
    <form class="margin-form" action="editar_usuario.php?uid=<?php echo $_GET['uid'] ?>" method="post">
        <div class="row mb-4">
            <div class="col-4">
                <label for="nombre">Id: <?php echo $id ?></label>
            </div>
            <div class="col-4">
                <label for="nombre">Nombre:</label>
                <input id="nombre" class="form-item" type="text" name="nombre" value="<?php echo $nombre ?>" placeholder="Nombre" required>
            </div>
            <div class="col-4">
                <img style="width: 17%;" src=<?php echo "img-perfil/" . $imagen ?> alt="foto perfil">
                <input id="img" class="form-item" accept=".png, .jpg" type="file" name="imagen">
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-4">
                <label for="nombre">Correo:</label>
                <input id="correo" class="form-item" type="email" name="correo" value="<?php echo $correo ?>" placeholder="Correo" required>
            </div>
            <div class="col-4">
                <label for="nombre">Perfil:</label>
                <select class="form-item selectores" name="perfil" id="perfil" require>
                    <?php if ($perfil == "Consulta") { ?>
                        <option selected value="Consulta">Consulta</option>
                        <option value="Administrador">Administrador</option>
                    <?php
                    } else if ($perfil == "Administrador") { ?>
                        <option selected value="Consulta">Administrador</option>
                        <option value="Administrador">Consulta</option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="col-4">
                <label for="usuario">Usuario:</label>
                <input id="usuario" class="form-item" type="text" name="usuario" value="<?php echo $usuario ?>" placeholder="Usuario" required>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-6">
                <div class="col-1"><label>Recetas guardadas:</label></div>
                <textarea id="receta_g" class="text-area" name="recetas_g" placeholder="Recetas guardadas"><?php echo $recetas_g ?></textarea>
            </div>
            <div class="col-6">
                <div class="col-1"><label>Recetas votadas:</label></div>
                <textarea id="receta_v" class="text-area" name="recetas_v" placeholder="Recetas votadas"><?php echo $recetas_v ?></textarea>
            </div>
        </div>
        <div class="row text-center mb-5 pb-5">
            <div class="col-6">
                <input class="btn-submit" type="button" onclick="history.back()" name="Atras" value="Atrás">
            </div>
            <div class="col-6">
                <input class="btn-submit" type="submit" name="actualizar" value="Actualizar">
            </div>
        </div>
    </form>
</main>