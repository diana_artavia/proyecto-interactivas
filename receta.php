    <title>Receta | Veggie Taste</title>
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="css/receta.css">
    <link rel="stylesheet" href="css/utils.css">
    <?php
    include_once('includes/header.php');
    

    $id = $_GET['id_receta'];
    $result = $database->select("receta_tb", "*", ["id_receta" => $id]);

    if (isset($_GET['vote'])) {
        if ($_GET['vote'] == 1) {
            //aumentar likes
            $likes = $result[0]['likes'] + 1;
            $update = $database->update("receta_tb", ["likes" => $likes], ["id_receta" => $id]);

            //agregar id a usuario
        } //fin del if
    } //fin del if
    $valoraciones = $result[0]['likes'] + $result[0]['dislikes'];

    if (isset($_GET['id_usuario'])) {
        $user = $database->select("usuario_tb", "*", ["uid" => $_GET['id_usuario']]);
        $recetas = $user[0]['recetas_g'];

        if ($recetas > 0) {
            $database->update("usuario_tb", [
                'recetas_g' => $recetas . "," . $id
            ], ['uid' => $user[0]['uid']]);
        } else {
            $database->update("usuario_tb", [
                'recetas_g' => $id
            ], ['uid' => $user[0]['uid']]);
        } //fin del else

        $usertemp = $database->select("usuario_tb", "*", ["uid" => $_GET['id_usuario']]);
        $recetastemp = $usertemp[0]['recetas_g'];
        $recetas_clean = array_unique(explode(",", $recetastemp));
        $database->update("usuario_tb", [
            'recetas_g' => implode(",", $recetas_clean)
        ], ['uid' => $usertemp[0]['uid']]);
    } //fin del if

    ?>

    <main>
        <section>
            <div class="row text-center">
                <div class="column-receta" data-aos="fade-right" data-aos-duration="1500">
                    <img class="img-80" src="<?php echo "imgRecetas/" . $result[0]['imagen'] ?>" alt="Receta">
                </div>
                <div class="column-receta">
                    <h3 class="text-green title-main"><?php echo $result[0]['nombre'] ?></h3>
                </div>
                <div class="column-3">
                    <?php
                    $resultado = $database->select("usuario_tb", "*");
                    for ($i = 0; $i < count($resultado); $i++) {
                        if ($resultado[$i]["session"] == "1") {
                            $id_usuario = $resultado[$i]['uid']; ?>
                    <?php }
                    }
                    ?>
                    <a href="receta.php?id_receta=<?php echo $id ?>&id_usuario=<?php echo $id_usuario ?>"><img class="img-favs" src="imgInicio/fav.png" alt="Guardar en favoritos"></a>
                </div>
                <div class="column-3 votos">
                    <h3 class="text-green"><img class="" src="imgInicio/destacada2.png" alt="Icono de estrella"></h3>
                    <p class="title-op text-green"><?php echo $valoraciones . " valoraciones" ?></p>
                </div>
            </div>
        </section>
        <section>
            <div class="row bg-green mb-5 mt-5">
                <?php
                if ($result[0]['destacada'] == "Si") { ?>
                    <div class="column-1">
                        <img class="pt-4" src="imgInicio/destacada1.png" alt="Icono destacado">
                        <p class="main-text pos-abs">Receta Destacada</p>
                    </div>
                <?php }
                ?>

                <div class="column-1">
                    <img class="pt-4" src="imgInicio/preparacion.png" alt="Icono reloj">
                    <p class="main-text pos-abs">Preparacion <span><?php echo $result[0]['tiempo_preparacion'] ?></span></p>
                </div>
                <div class="column-1">
                    <img class="pt-4" src="imgInicio/coccion.png" alt="Icono reloj">
                    <p class="main-text pos-abs">Cocción <span><?php echo $result[0]['tiempo_coccion'] ?></span></p>
                </div>
                <div class="column-1">
                    <img class="pt-4 pb-4" src="imgInicio/tiempo1.png" alt="Icono reloj">
                    <p class="main-text pos-abs">Total <span><?php echo $result[0]['tiempo_total'] ?></span></p>
                </div>
            </div>
            <div class="row bg-green mb-5">
                <div class="column-1">
                    <img class="pt-4" src="imgInicio/complejidad.png" alt="Icono destacado">
                    <p class="main-text pos-abs">Complejidad <span><?php echo $result[0]['complejidad'] ?></span></p>
                </div>
                <div class="column-1">
                    <img class="pt-4" src="imgInicio/categoria.png" alt="Icono reloj">
                    <p class="main-text pos-abs">Categoria <span><?php echo $result[0]['categoria'] ?></span></p>
                </div>
                <div class="column-1">
                    <img class="pt-4" src="imgInicio/ocasion.png" alt="Icono reloj">
                    <p class="main-text pos-abs">Ocasión <span><?php echo $result[0]['ocasion'] ?></span></p>
                </div>
                <div class="column-1">
                    <img class="pt-4 pb-4" src="imgInicio/porciones.png" alt="Icono reloj">
                    <p class="main-text pos-abs">Porciones <span><?php echo $result[0]['porciones'] ?></span></p>
                </div>
            </div>
        </section>
        <section>
            <div class="row container mb-3 gap-5 ">
                <div class="column-4">
                    <h3 class="text-green fs-titles">Descripción</h3>
                    <p class="fs-main"><?php echo $result[0]['descripcion'] ?></p>
                </div>
                <div class="column-4">
                    <h3 class="text-green fs-titles">Lista de ingredientes</h3>
                    <div class="fs-main ml-3">

                        <?php echo "<li>" . str_replace(",", "<li>", $result[0]['ingredientes']) ?>
                    </div>
                </div>
            </div>
            <div class="row container mb-3">
                <div class="column-2">
                    <h3 class="text-green fs-titles">Instrucciones</h3>
                    <div class="fs-main ml-3">

                        <?php
                        echo "<li>" . str_replace(". ", "<li>", $result[0]['instrucciones']);
                        ?>
                    </div>
                </div>
            </div>

            <?php
            $sesion = 0;
            $resultado = $database->select("usuario_tb", "*");
            for ($i = 0; $i < count($resultado); $i++) {
                if ($resultado[$i]["session"] == "1") {
                    $id_usuario = $resultado[$i]['uid']; ?>

                    <div class="row container mb-3">
                        <div class="column-2 text-center">
                            <h3 class="text-green fs-titles mb-4">Valora esta receta</h3>
                            <a href="receta.php?id_receta=<?php echo $id ?>&id_usuario=<?php echo $id_usuario ?>&vote=1" class="fa faa fa-thumbs-up"></a>
                        </div>
                    </div>

            <?php $sesion = "1";
                }
            }
            ?>


        </section>
        <section>
            <h3 class="text-green text-center fs-titles">Recetas similares</h3>
            <div class="row-card">
                <?php include_once('includes/similar_cards.php'); ?>
            </div>
        </section>
        <section>
            <div class="text-center">
                <img class="img-25 pt-5 pb-5" src="imgInicio/hoja2.png" alt="Hoja decorativa">
            </div>
        </section>
    </main>

    <?php
    include_once 'includes/header.php'
    ?>

    <script src="js/receta.js"></script>
    <script src="js/topNav.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>
    </body>

    </html>