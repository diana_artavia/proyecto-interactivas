<?php
include_once("bd.php");

$id = $_GET['id_receta'];
$result = $database->select("receta_tb", "*", ["id_receta" => $id]);
$categoria = $result[0]['ocasion'];

$resultado = $database->select("usuario_tb", "*");
for ($i = 0; $i < count($resultado); $i++) {
    if ($resultado[$i]["session"] == "1") {
        $id_usuario = $resultado[$i]['uid']; ?>
    <?php }
}

if (isset($_GET['id_usuario']) && isset($_GET['id_receta'])) {
    $id = $_GET['id_receta'];
    $user = $database->select("usuario_tb", "*", ["uid" => $_GET['id_usuario']]);
    $recetas = $user[0]['recetas_g'];

    if ($recetas > 0) {
        $database->update("usuario_tb", [
            'recetas_g' => $recetas . "," . $id
        ], ['uid' => $user[0]['uid']]);
    } else {
        $database->update("usuario_tb", [
            'recetas_g' => $id
        ], ['uid' => $user[0]['uid']]);
    } //fin del else

    $usertemp = $database->select("usuario_tb", "*", ["uid" => $_GET['id_usuario']]);
    $recetastemp = $usertemp[0]['recetas_g'];
    $recetas_clean = array_unique(explode(",", $recetastemp));
    $database->update("usuario_tb", [
        'recetas_g' => implode(",", $recetas_clean)
    ], ['uid' => $usertemp[0]['uid']]);
} //fin del if

if ($result[0]['categoria'] == "Bebidas") {
    $result = $database->select("receta_tb", "*", ["categoria" => "Bebidas"]);
    for ($i = 0; $i < count($result); $i++) {
        if ($result[$i]['id_receta'] != $id) {
?>
            <div class="column" data-aos="fade-in" data-aos-delay="300" data-aos-offset="0">
                <div class="card">
                    <img class="img-100 m-auto" src=<?php echo "imgRecetas/" . $result[$i]['imagen'] ?> alt="...">
                    <h3 class="pb-5 pt-4"><?php echo $result[$i]['nombre'] ?></h3>
                    <a class="btn-card" href="receta.php?id_receta=<?php echo $result[$i]['id_receta'] ?>">Ver</a>
                    <a href="receta.php?id_receta=<?php echo $result[$i]['id_receta'] ?>&id_usuario=<?php echo $id_usuario ?>"><img class="img-fav" src="imgInicio/fav.png" alt="Guardar en favoritos"></a>
                </div>
            </div>
        <?php
        }
    }
} else if ($result[0]['categoria'] == "Postres") {
    $result = $database->select("receta_tb", "*", ["categoria" => "Postres"]);
    for ($i = 0; $i < count($result); $i++) {
        if ($result[$i]['id_receta'] != $id) {
        ?>
            <div class="column" data-aos="fade-in" data-aos-delay="300" data-aos-offset="0">
                <div class="card">
                    <img class="img-100 m-auto" src=<?php echo "imgRecetas/" . $result[$i]['imagen'] ?> alt="...">
                    <h3 class="pb-5 pt-4"><?php echo $result[$i]['nombre'] ?></h3>
                    <a class="btn-card" href="receta.php?id_receta=<?php echo $result[$i]['id_receta'] ?>">Ver</a>
                    <a href="receta.php?id_receta=<?php echo $result[$i]['id_receta'] ?>&id_usuario=<?php echo $id_usuario ?>"><img class="img-fav" src="imgInicio/fav.png" alt="Guardar en favoritos"></a>
                </div>
            </div>
        <?php
        }
    }
} else if ($result[0]['categoria'] == "Almuerzo") {
    $result = $database->select("receta_tb", "*", ["categoria" => "Almuerzo"]);
    for ($i = 0; $i < count($result); $i++) {
        if ($result[$i]['id_receta'] != $id) {
        ?>
            <div class="column" data-aos="fade-in" data-aos-delay="300" data-aos-offset="0">
                <div class="card">
                    <img class="img-100 m-auto" src=<?php echo "imgRecetas/" . $result[$i]['imagen'] ?> alt="...">
                    <h3 class="pb-4 pt-3"><?php echo $result[$i]['nombre'] ?></h3>
                    <a class="btn-card" href="receta.php?id_receta=<?php echo $result[$i]['id_receta'] ?>">Ver</a>
                    <a href="receta.php?id_receta=<?php echo $result[$i]['id_receta'] ?>&id_usuario=<?php echo $id_usuario ?>"><img class="img-fav" src="imgInicio/fav.png" alt="Guardar en favoritos"></a>
                </div>
            </div>
        <?php
        }
    }
} else if ($result[0]['categoria'] == "Sopas") {
    $result = $database->select("receta_tb", "*", ["categoria" => "Sopas"]);
    for ($i = 0; $i < count($result); $i++) {
        if ($result[$i]['id_receta'] != $id) {
        ?>
            <div class="column" data-aos="fade-in" data-aos-delay="300" data-aos-offset="0">
                <div class="card">
                    <img class="img-100 m-auto" src=<?php echo "imgRecetas/" . $result[$i]['imagen'] ?> alt="...">
                    <h3 class="pb-4 pt-3"><?php echo $result[$i]['nombre'] ?></h3>
                    <a class="btn-card" href="receta.php?id_receta=<?php echo $result[$i]['id_receta'] ?>">Ver</a>
                    <a href="receta.php?id_receta=<?php echo $result[$i]['id_receta'] ?>&id_usuario=<?php echo $id_usuario ?>"><img class="img-fav" src="imgInicio/fav.png" alt="Guardar en favoritos"></a>
                </div>
            </div>
        <?php
        }
    }
} else if ($result[0]['categoria'] == "Entradas") {
    $result = $database->select("receta_tb", "*", ["categoria" => "Entradas"]);
    for ($i = 0; $i < count($result); $i++) {
        if ($result[$i]['id_receta'] != $id) {
        ?>
            <div class="column" data-aos="fade-in" data-aos-delay="300" data-aos-offset="0">
                <div class="card">
                    <img class="img-100 m-auto" src=<?php echo "imgRecetas/" . $result[$i]['imagen'] ?> alt="...">
                    <h3 class="pb-4 pt-3"><?php echo $result[$i]['nombre'] ?></h3>
                    <a class="btn-card" href="receta.php?id_receta=<?php echo $result[$i]['id_receta'] ?>">Ver</a>
                    <a href="receta.php?id_receta=<?php echo $result[$i]['id_receta'] ?>&id_usuario=<?php echo $id_usuario ?>"><img class="img-fav" src="imgInicio/fav.png" alt="Guardar en favoritos"></a>
                </div>
            </div>
        <?php
        }
    }
} else {
    $result = $database->select("receta_tb", "*", ["categoria" => "Desayuno"]);
    for ($i = 0; $i < count($result); $i++) {
        if ($result[$i]['id_receta'] != $id) {
        ?>
            <div class="column" data-aos="fade-in" data-aos-delay="300" data-aos-offset="0">
                <div class="card">
                    <img class="img-100 m-auto" src=<?php echo "imgRecetas/" . $result[$i]['imagen'] ?> alt="...">
                    <h3 class="pb-4 pt-3"><?php echo $result[$i]['nombre'] ?></h3>
                    <a class="btn-card" href="receta.php?id_receta=<?php echo $result[$i]['id_receta'] ?>">Ver</a>
                    <a href="receta.php?id_receta=<?php echo $result[$i]['id_receta'] ?>&id_usuario=<?php echo $id_usuario ?>"><img class="img-fav" src="imgInicio/fav.png" alt="Guardar en favoritos"></a>
                </div>
            </div>
<?php
        }
    }
}
?>