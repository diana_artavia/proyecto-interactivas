<?php
include_once("includes/bd.php");
?>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="css/front.css">
<link rel="stylesheet" href="css/login.css">

<nav id="topnav" class="top-nav">
    <a class="active" href="./home.php" title="Inicio">Inicio</a>
    <a href="./buscar.php" title="Buscar">Buscar</a>
    <?php
        $sesion = 0;
        $result = $database -> select("usuario_tb", "*");
        for($i = 0; $i < count($result); $i++){
            if($result[$i]["session"] == "1"){ ?>
               <a href="./perfil.php" title="Perfil">Perfil</a> 
            <?php $sesion = "1"; }
        }
        if($sesion == 0){ ?>
            <a href="./login.php" title="Login">Iniciar sesión</a> 
        <?php }
    ?> 
    <a href="javascript:void(0);" class="icon" onclick="topNavMenu()">
        <i class="fa fa-bars"></i>
</nav>

<section class="logo mb-5">
    <a href="./home.html"><img class="img-logo" src="imgInicio/LogoVT.png" alt="Logo Veggie Taste"></a>
</section>

