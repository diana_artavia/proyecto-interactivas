<?php
include_once("includes/bd.php");

$resultado = $database->select("usuario_tb", "*");
for ($i = 0; $i < count($resultado); $i++) {
    if ($resultado[$i]["session"] == "1") {
        $id_usuario = $resultado[$i]['uid']; ?>
    <?php }
}

if (isset($_GET['id_usuario']) && isset($_GET['id_receta'])) {
    $id = $_GET['id_receta'];
    $user = $database->select("usuario_tb", "*", ["uid" => $_GET['id_usuario']]);
    $recetas = $user[0]['recetas_g'];

    if ($recetas > 0) {
        $database->update("usuario_tb", [
            'recetas_g' => $recetas . "," . $id
        ], ['uid' => $user[0]['uid']]);
    } else {
        $database->update("usuario_tb", [
            'recetas_g' => $id
        ], ['uid' => $user[0]['uid']]);
    } //fin del else

    $usertemp = $database->select("usuario_tb", "*", ["uid" => $_GET['id_usuario']]);
    $recetastemp = $usertemp[0]['recetas_g'];
    $recetas_clean = array_unique(explode(",", $recetastemp));
    $database->update("usuario_tb", [
        'recetas_g' => implode(",", $recetas_clean)
    ], ['uid' => $usertemp[0]['uid']]);
} //fin del if

if (isset($_POST["buscar"])) {

    $nombre = $_POST['fname'];
    $categoria = $_POST['categoria'];
    $complejidad = $_POST['complejidad'];
    $ocasion = $_POST['ocasion'];

    if ($categoria == "Bebidas" || $categoria == "Postres" || $categoria == "Almuerzo" || $categoria == "Sopas" || $categoria == "Entradas" || $categoria == "Desayuno") {

        $result = $database->select("receta_tb", "*", ["categoria" => $categoria]);

        for ($i = 0; $i < count($result); $i++) {
?>
            <div class="column" data-aos="fade-in" data-aos-delay="300" data-aos-offset="0">
                <div class="card">
                    <img class="img-100 m-auto" src=<?php echo "imgRecetas/" . $result[$i]['imagen'] ?> alt="...">
                    <h3 class="pb-5 pt-4"><?php echo $result[$i]['nombre'] ?></h3>
                    <a class="btn-card" href="receta.php?id_receta=<?php echo $result[$i]['id_receta'] ?>">Ver</a>
                    <a href="buscar.php?id_receta=<?php echo $result[$i]['id_receta'] ?>&id_usuario=<?php echo $id_usuario ?>"><img class="img-fav" src="imgInicio/fav.png" alt="Guardar en favoritos"></a>
                </div>
            </div>
<?php
        }//fin del for
    }//fin del if categoria

    if ($complejidad == "Fácil" || $complejidad == "Intermedio" || $complejidad == "Avanzado") {

        $result = $database->select("receta_tb", "*", ["complejidad" => $complejidad]);

        for ($i = 0; $i < count($result); $i++) {
?>
            <div class="column" data-aos="fade-in" data-aos-delay="300" data-aos-offset="0">
                <div class="card">
                    <img class="img-100 m-auto" src=<?php echo "imgRecetas/" . $result[$i]['imagen'] ?> alt="...">
                    <h3 class="pb-5 pt-4"><?php echo $result[$i]['nombre'] ?></h3>
                    <a class="btn-card" href="receta.php?id_receta=<?php echo $result[$i]['id_receta'] ?>">Ver</a>
                    <a href="buscar.php?id_receta=<?php echo $result[$i]['id_receta'] ?>&id_usuario=<?php echo $id_usuario ?>"><img class="img-fav" src="imgInicio/fav.png" alt="Guardar en favoritos"></a>
                </div>
            </div>
<?php
        }//fin del for
    }//fin del if complejidad
    
    if ($ocasion == "Todas" || $ocasion == "Cumpleaños" || $ocasion == "Día de la madre" || $ocasion == "Día del padre" || $ocasion == "Día de la madre" || $ocasion == "Día del niño" || $ocasion == "Navidad" || $ocasion == "Semana Santa" || $ocasion == "Verano") {

        $result = $database->select("receta_tb", "*", ["ocasion" => $ocasion]);

        for ($i = 0; $i < count($result); $i++) {
?>
            <div class="column" data-aos="fade-in" data-aos-delay="300" data-aos-offset="0">
                <div class="card">
                    <img class="img-100 m-auto" src=<?php echo "imgRecetas/" . $result[$i]['imagen'] ?> alt="...">
                    <h3 class="pb-4 pt-3"><?php echo $result[$i]['nombre'] ?></h3>
                    <a class="btn-card" href="receta.php?id_receta=<?php echo $result[$i]['id_receta'] ?>">Ver</a>
                    <a href="buscar.php?id_receta=<?php echo $result[$i]['id_receta'] ?>&id_usuario=<?php echo $id_usuario ?>"><img class="img-fav" src="imgInicio/fav.png" alt="Guardar en favoritos"></a>
                </div>
            </div>
<?php
        }//fin del for
    }//fin del if ocasion

    $result = $database->select("receta_tb", "*", ["nombre" => $nombre]);

    if ($result != null) {
        for ($i = 0; $i < count($result); $i++) {
?>
            <div class="column" data-aos="fade-in" data-aos-delay="300" data-aos-offset="0">
                <div class="card">
                    <img class="img-100 m-auto" src=<?php echo "imgRecetas/" . $result[$i]['imagen'] ?> alt="...">
                    <h3 class="pb-4 pt-3"><?php echo $result[$i]['nombre'] ?></h3>
                    <a class="btn-card" href="receta.php?id_receta=<?php echo $result[$i]['id_receta'] ?>">Ver</a>
                    <a href="buscar.php?id_receta=<?php echo $result[$i]['id_receta'] ?>&id_usuario=<?php echo $id_usuario ?>"><img class="img-fav" src="imgInicio/fav.png" alt="Guardar en favoritos"></a>
                </div>
            </div>
<?php
        }//fin del for
    }//fin del if ocasion
} //fin del if(isset($_POST["buscar"]))
?>