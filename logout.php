<?php
include_once ("includes/bd.php");

    session_start();

    $result = $database -> select("usuario_tb", "*");

    if(session_destroy()){
        for($i = 0; $i < count($result); $i++){
            if($result[$i]["session"] == "1"){
                $result = $database -> update("usuario_tb", ["session" => "0"], $result[$i]);    
            }//if
        }//fin del for

        header("Location: index.php");
    }//fin del if

?>