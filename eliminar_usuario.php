<?php
include_once("includes/bd.php");
include_once("session.php");

if(isset($_GET['uid'])){
    $result = $database -> delete("usuario_tb", ["uid" => $_GET['uid']]);

    if(!$result){
        die("Query Failed");
    }
    
    $_SESSION['mensaje'] = " Usuario eliminado correctamente!";
    $_SESSION['mensaje_tipo'] = "danger";

    header("location: usuarios.php");
}
?>